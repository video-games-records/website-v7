# Video Games Records V7 Changelog


## 7.0.40 - 02 September 2023
- Fix: Team games order by rank
- Fix: Display player stats with no score
- Add: Message for connected users
- Add: Display proof form after submitted a score on single chart
- Add: VGR youtube link on menu (Videos)
- Add: All players of team will receive the notification of losing master badge if they have score on the game
- Add: A group can now be unranked

## 7.0.39 - 02 September 2023
- Add: Order by point Chart on advance search
- Add: My notified topics
- Add: DLC icon on group
- Add: Series with leaderboard and badge
- Add: Submit scores by game with score filter


## 7.0.38 - XX
- Fix: Link on video list
- Add: Private message to player when losing player master badge
- Add: Private message to leader when losing team master badge
- Add: Auto set platform on score if you have only one platorm setted on the game
- Add: Lastest submitted scores with picture proof on player's profile
- Add: Lastest submitted scores with video proof on player's profile
- Add: You can now edit your comment on news
- Delete: IsOnline status


## 7.0.37 - 19 february 2023

- Fix: Switch group/form data initialize after refresh page
- Add: Update video form
- Add: Previous / next link on aside switch group 
- Add: Previous / next link on aside switch record
- Add: Teams top 5 players leaderboards
- Add: Display team's presentaion (Update form for leader)
- Add: Download URL for android / ios games

## 7.0.36 - 03 february 2023

- Fix: Disabled scores are in good order
- Fix: Wrong number of private message not read after delete/read message
- Add: Display badge earned date on rollover
- Add: Display gamecard on player's profile
- Add: Display lastest submitted scores on player's profile
- Add: Serie link on game page
- Add: Total chart on each player's profile games
- Add: Display game's platforms on serie

## 7.0.35 - 21 january 2023

- Fix: Send proof video
- Add: Display Sent date on private message
- Add: Filter by game's platforms on advance search
- Add: Average game rank on player's profile
- Add: Average game rank on team's profile
- Add: Number of chart with link platform on player's profile
- Add: Percentage of submit score on player's profile
- Add: Percentage of score with platform on player's profile
- Fix: Order cup on team's profile
- Fix: Series on alpha order
- Fix: Logout after 30 mins
- Fix: No limit on series
- Add: Proof pourcentage on player's profile

## 7.0.34 - 26 december 2022

- Add: Chekbox accept rules for new users
- Add: Message on register form
- Add: Serie ranking
- Change: Upcoming games list by status

## 7.0.33 - 27 february 2022

- Fix: Platinum cup on team's profile
- Fix: Title and description on player's profile, team's profile and advanced search
- Fix: Submit score button is always visible
- Fix: New games topic messages are now at the top of Games forum
- Fix: Country badges tooltip displays country name instead of badge ID
- Change: Profile statistics are not displayed if player is less than 1 day old (there is no data to display on the first day)

## 7.0.32 - 3 february 2022

- Fix: General forum is not shown
- Fix: Sometimes a game doesn't count on player stats after update
- Fix: Language change on record page
- Change: Proof picture is upgraded to 1080 pixels
- Add: Add message after reaching daily proof request limit

## 7.0.31 - 29 august 2021

- Fix: Infinite loading of global statistics
- Fix: Proofs ranking not updated
- Change: Platform ranking now takes into account scores with declared platform only
- Change: Discord logo in sidebar
- Add: Shop link in sidebar
- Add: Platform badges

## 7.0.30 - 22 august 2021

- Fix: Game/group/record names not translated when switching language
- Fix: Website link not working on player's profile
- Change: Several improvements in admin
- Add: Record field in advanced score search
- Add: Game of the day is now random when there is no game defined manually

## 7.0.29 - 15 august 2021

- Fix: When editing multiple scores on a group, blue square proof were removed even if the corresponding score wasn't updated
- Fix: Update ranking after score edition from administration
- Fix: Missing score link in breadcrumb when we're on score submission page
- Change: Filters on profile's Score tab moved to Advanced score search
- Add: Advanced score search with players filter
- Add: Date and hour of last score update on the score page
- Add: Starting date of investigation of a score on the score page
- Add: Ability to view proofs sent from Scores tab in profile
- Add: Styles for Block quote in text editor
- Add: Text editor's guide [on the forum](https://www.video-games-records.com/en/the-site-forum-f1/text-editor-s-guide-guide-de-l-editeur-de-texte-topic-t14165/index)

## 7.0.28 - 1 august 2021

- Fix: Dates of medals graphic in profile
- Fix: Alphabetical order of games in Games filter on Scores tab in profile
- Fix: Alphabetical order of games in games list by plateform
- Fix: Order of scores in submission form
- Fix: Layout of deactivated scores
- Change: Deactivation of [automatic text transformation](https://ckeditor.com/docs/ckeditor5/latest/features/text-transformation.html) in text editor
- Add: Gradient effect on line background for 1st, 2nd and 3rd places in leaderboards
- Add: Player's avatar for 1st, 2nd and 3rd places in leaderboards
- Add: Team's avatar for 1st, 2nd and 3rd places in leaderboards

## 7.0.27 - 25 july 2021

- Fix: Topic keeps unread after replying to it
- Fix: Unicode compatibility in database
- Change: Various typographic updates
- Change: Header banner
- Add: Platforms on Master Badge details

## 7.0.26 - 4 july 2021

- Fix: Display Twitch video
- Change: A game can now be unranked for all leaderboards
- Add: New page for lastest games
- Add: Game Points and Record Points earned with a Master badge (on Master badge detail)
- Add: Obsolete lost positions are now deleted automatically once a day
- Add: Hour of registration and last connection in player profile 
- Add: Ability to filter scores by rank, by Game Points, by platinum medal and on multiple games (in profile Scores tab)

## 7.0.25 - 28 june 2021

- Fix: Order by rank with platinum and gold on player games profile
- Fix: Team Cup leaderboard is not updated
- Fix: Read form  
- Add: Return of Team forum on forum pages

## 7.0.24 - 20 june 2021

- Fix: Games sub-forums are back in the forum
- Fix: Mark all topics as read in a forum
- Fix: Video search does not filter on value
- Fix: Alphabetic order in games search results
- Fix: Text editor not responsive as expected on mobile
- Fix: Several translations issues
- Add: New translation strings in global translation files

## 7.0.23 - 12 june 2021

- Fix: Preview for media embedded (YouTube, Vimeo, Dailymotion, Spotify)
- Fix: Layout of text editor toolbar when fixed on top of screen
- Change: Preparing better translation management to easily add new languages in the future
- Add: Headings and Media buttons in editor
- Add: Preparing all Platforms badges

## 7.0.22 - 30 may 2021

- Fix: Switch group and record
- Fix: Forum don't load for new readers
- Fix: Online/offline status on forum
- Fix: Search on members now returns active members only
- Fix: Direct redirection to the posted message after adding it on forum
- Fix: Forum notification URL is now directly redirecting to the corresponding message
- Fix: New lost position
- Fix: Button to delete proof on score page
- Change: Sender of forum notification is now set to user who has posted the message, so you can filter on it if needed.
- Add: Send notification when a notified topic is updated on forum

### Text editor

- Add: Ability to strike out text in messages
- Add: Ability to remove format from text (useful if you copy some text from external source)
- Add: Ability to add images from a URL in messages (forum, private messages, account)
- Add: Ability to add an alternate text and a link on images (clicking on it in editor)
- Add: Ability to manage row/line/cell properties on tables
- Add: Ability to add emojis in messages

## 7.0.21 - 22 may 2021

- Fix: Order of equal golds position depending of status and date
- Fix: Top score layout if no personal score on a record
- Change: Display number of members if equality on top score
- Add: Records Points on scores list
- Add: Ability to delete proof on score page
- Add: Positions chart on player's profile
- Add: Medals by time chart on player's profile
- Add: Proof stats on my proofs page
- Add: Open proof picture within a modal on proofs list

## 7.0.20 - 16 may 2021

- Fix: Home page leaderboards
- Fix: Team's Master badges ordering
- Change: Top score layout (each data has its own column)
- Change: Wider browser support to try to improve display on older browsers
- Add: Game Points in points game rankings ("Records Points" is replaced by "Points" in tab name accordingly)
- Add: Ability to delete proofs
- Add: Ability to select / unselect all lost positions
- Add: Role of admin members in players list
- Add: Filter by platform on player's games list
- Add: Filter inbox private messages by user
- Add: Filter outbox private  messages by object and user
- Add: Slug for article pages

## 7.0.19 - 8 may 2021

- Fix: 3 lastest games on home page
- Fix: Order by name on game list by platform and My proofs page
- Fix: Order of records on My proofs page
- Fix: Sidebar display on the right on large screens
- Fix: Platinum cups on Games list in player's profile
- Fix: Broken rankings on games when switching language
- Change: Score can me modified only if status prooved or not prooved
- Change: Ability to post messages on Announcement messages on forum
- Change: Various layout changes in Account pages
- Add: Order Master Badges for player and team
- Add: Notifications added in private messages for proofs and ask for proof
- Add: Type and Object filters added on private messages
- Add: User profile link on forum
- Add: Online/offline status on player's list and profile, so as comments and forum

## 7.0.18 - 2 may 2021

- Fix: Switch language on article
- Change: Platform names truncated if too long on games displayed on home page
- Change: Game of day's platform with orange border too
- Change: Layout improvement on lost medals
- Add: Show all status on player's proof stats (general and per game)
- Add: Show Deactivated score on chart view
- Add: Position on player's list
- Add: File robots.txt

## 7.0.17 - 25 april 2021

### Front

- Fix: Message order on player's profile
- Fix: Translation of titles of articles on home page when switching language
- Change: Stay on same page when switching language
- Change: Management of error/success notifications
- Add: Show an error if a problem occurs when uploading video

### Admin

- Fix: Installation of a new testing environment (old one lost in OVH data center fire in March)
- Fix: Sorting option on games/group/records in proofs list
- Fix: Game copy
- Change: Edit Master badge from game
- Change: Improvements of interface to easier add and edit games

## 7.0.16 - 10 april 2021

- Fix: Rules link on My proofs
- Change: Improve and harmonize various words accross the site
- Change: "Chart" is now replaced by "Record" in English
- Change: Button to edit a score also on top of records list and detail
- Add: Game topics and messages
- Add: Players list

## 7.0.15 - 3 april 2021

- Fix: Layout of weekly/monthly/yearly tops on mobile
- Fix: Cache update of navigation script
- Change: Links to full lists or contents in bold on home page
- Add: Game of day on home page
- Add: Link to games list on home page
- Add: Link to all articles on home page
- Add: Links to top 100 under leaderboard on home page
- Add: Proof status on My proofs 
- Add: Display gender and age on player's profile (an option in account allows to choose to display them or not)
- Add: Prepare new platforms Commodore 64 Mini, Neo Geo Mini and Neo Geo Arcade Stick Pro

## 7.0.14 - 27 march 2021

- Fix: Missing tooltips in top bar
- Fix: Translation in main menu when switching language
- Fix: Rank over 99 displayed on 2 lines on chart ranking
- Fix: Bug on group and chart switches
- Fix: Master badge value not updated
- Change: Simplify main menu code
- Change: Rename and reorder Rankings menu
- Add: Team's tag on player's profile
- Add: Record of maximum number of users connected
- Add: Button to open all groups on proofs of a game
- Add: Number of lost medals on top bar
- Add: Reminder of rules and espacially deactivated score limit on My proofs
- Add: Message if no score sent in a group on My proofs
- Add: Highlight line of connected player and team in leaderboards

## 7.0.13 - 21 march 2021

- Fix: Several notifications for one lost medal
- Change: Anonymization of IP for Google Analytics
- Add: Link to the game index on game picture
- Add: My proofs page accessible from top bar
- Add: Ability to drag and drop proofs directly on proofs listed by game
- Add: Tooltips on top bar icons

## 7.0.12 - 13 march 2021

- Fix: Localize numbers accross all pages
- Fix: Medals filter on player's profile
- Fix: Medals filter on team's profile
- Fix: Decrement message numbers after opening an unread message
- Change: Display welcome message to non connected users only
- Change: Display a message to explain why old video proofs are not shown
- Change: Better tooltip on proof status
- Add: Display leader on team's profile
- Add: Display last update on chart ranking
- Add: Display score's number per game on player's profile
- Add: Update private message's number every 5 minutes
- Add: Button Select all / Unselect all on private messages

## 7.0.11 - 6 march 2021

- Fix: Better names on badges (displayed on mouse over)
- Fix: Platinum cups on profile player's profile
- Fix: Platinum cups on profile team's profile
- Fix: Don't show private messages on player's profile
- Fix: Colors for some platforms
- Change: Improve display of numbers in several places
- Add: Show proof form for unproven scores
- Add: Proofs on player's profile

## 7.0.10 - 27 february 2021

- Fix: Error when submitting several scores at the same time
- Fix: Platform link redirects to the corresponding game list
- Fix: Display only active platforms in platforms list
- Fix: Badge ranking calculation
- Fix: Scroll in mobile menu
- Change: Minimum caracters required in search field is 3 instead of 4
- Change: Display 4 latest games added as image instead of a list
- Change: Display 6 articles instead of 10 on home page
- Change: Harmonize various labels such as Game Points, Badge Points, etc
- Change: Display editor to write private message in modal window
- Add: Display platinum cups on player and team profile
- Add: User's messages in profile
- Add: Filters on games list on player and team profile
- Add: All games/status option in Charts in player's profile
- Add: Master badge history on player's profile
- Add: Ability to send private message directly on player's profile
- Add: Flag of player's country in profile
- Add: Direct access to public profile in top bar

## 7.0.9 - 19 february 2021

- Fix: Color for Super Nintendo Mini / Super Famicom Mini and Nintendo Mini / Famicom Mini
- Change: Improve mobile search
- Change: Improve score detail page layout
- Change: Improve layout of player's profile on small screens
- Change: Improve layout of tabs on small screens
- Change: Presentation and Collection form edition in account
- Add: Charts tabs on player's profile with filter by game and status
- Add : Donation, Chart, Proof and Special badges on player's profile
- Add: Ability to see proof when clicking on proof status in chart list
- Add: Highlight orderable columns (for now only on games by platform list and charts in player's profile)
- Add: Creation year in the home message

## 7.0.8 - 12 february 2021

- Fix: Error when submitting several scores at the same time
- Change: Edit bouton on group displayed as button
- Change: Layout of submit score page improved
- Change: Latest article displayed on top of leaderboard on home
- Add: Latest submissions
- Add: Presentation and Collection in player's profile
- Add: YouTube share link can be used to submit a video
- Add: New platforms (Xbox Series, PlayStation 5, Megadrive Mini / Genesis Mini, Super Nintendo Mini / Super Famicom Mini, Nintendo Mini / Famicom Mini, PlayStation Classic)

## 7.0.7 - 6 february 2021

- Fix: Broken layout of chart list
- Fix: New message position in forum
- Fix: Status display broken in forum message
- Change: Break lines in tutorial message on home page
- Change: Edit bouton on chart list displayed as button
- Change: Readme with details to install project locally
- Add: Some data in profile (status, number of connections, links to website, YouTube and Twitch)
- Add: Missing button to reply to direct message
- Add: Changelog linked in the footer

## 7.0.6 - 5 february 2021

### Forum

- Fix: Add to favorites
- Fix: Topics are distinguished according to their type
- Fix: Topics can be archived by admins (always accessible via search)
- Fix: Smarter word break
- Change: Improve display of new messages
- Add: Viewing member role

### Scoring

- Fix: Proof resquest form
- Fix: Platinum medal if only one player
- Add: Display video proof on player's score

### Private mesages

- Change: Improve display of new messages

## 7.0.5 - 30 january 2021

- Fix: The last message from a forum does not update when next topic is created
- Fix: Return of colored squares to specify the status of a score
- Change: The list of videos is simple and paginated, ordered by descending add
- Change: Top score are in alphabetic order
- Add: The list of videos is accessible from a game page
- Add: New private messages have a small icon
- Add: Return of connected users list today on forum

## 7.0.4 - 25 january 2021

- Fix: Can't add comment on news

## 7.0.3 - 24 january 2021

- Fix: The confirmation URL account created got a 404
- Fix: Comments on articles are not displayed properly
- Fix: Weekly top is not displayed properly
- Add: Article detail doesn't show publication date

## 7.0.2 - 23 january 2021

- Fix: Group chart list is limited to 30
- Fix: Group chart list is not ordered
- Fix: Missing chart link on top score
- Fix: An edited score has always PROOVE status with its picture
- Fix: Forum layout breaks if the content is too large

## 7.0.1 - 23 january 2021

- Fix: Can't submit score
- Fix: Can't add avatar
- Fix: Proof picture are not displayed
- Fix: Can't update account profile
- Fix: Wrong URL on subscription email and reset password email
- Fix: Players' profile with no country are not displayed
- Fix: Some spelling fixes

## 7.0.0 - 22 january 2021

- New: Platform rankin
- New: YouTube and Twitch video
- New: Responsive layout