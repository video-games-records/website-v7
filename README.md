# Video Games Records V7

This is the repository for front-end of the version 7 of [Video Games Records](https://www.videogamesrecords.net).

## Install

1. Clone the repository
2. Run `npm install` to install dependencies
3. That's it

## Run it locally

1. Run `npm run serve`
2. That's it

Note: to connect to development API, you can replace `VUE_APP_ROOT_API` value by `http://backoffice.vgr.projet-normandie.fr/api` (instead of `http://localhost:8000`).