import { mapMutations } from 'vuex';

export default {
    methods: {
        ...mapMutations('breadcrumbs', {
            setBreadcrumbOnlyItem1: 'setOnlyItem1',
            setBreadcrumbItem1: 'setItem1',
            setBreadcrumbItem2: 'setItem2',
            setBreadcrumbItem3: 'setItem3',
            setBreadcrumbItem4: 'setItem4',
            setBreadcrumbItem5: 'setItem5',
            setBreadcrumbLevel: 'setLevel',
        })
    }
}
