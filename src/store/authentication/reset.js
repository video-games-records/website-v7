import AuthApi from '../../services/auth.service';

export default {
    namespaced: true,
    state: {
        isLoading: false,
        success : null,
        message: null,
    },
    getters: {
        isLoading (state) {
            return state.isLoading;
        },
        success (state) {
            return state.success;
        },
        message (state) {
            return state.message;
        },
    },
    mutations: {
        ['LOADING'](state) {
            state.isLoading = true;
            state.success = null;
            state.error = null;
        },
        ['SUCCESS'](state, res) {
            state.isLoading = false;
            state.success = res.data.success;
            state.message = res.data.message;
        },
        ['ERROR'](state, error) {
            state.isLoading = false;
            state.success = false;
            state.error = error;
        },
    },
    actions: {
        sendEmail ({commit}, payload) {
            commit('LOADING');
            return AuthApi.resettingSendEmail(payload.email)
                .then(res => commit('SUCCESS', res))
                .catch(err => commit('ERROR', err));
        },
        reset ({commit}, payload) {
            commit('LOADING');
            return AuthApi.resettingReset(payload.token, payload.password)
                .then(res => commit('SUCCESS', res))
                .catch(err => commit('ERROR', err));
        },
    },
}
