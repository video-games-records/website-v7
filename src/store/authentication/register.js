import AuthApi from '../../services/auth.service';

export default {
    namespaced: true,
    state: {
        isLoading: false,
        success : null,
        message: null,
    },
    getters: {
        isLoading (state) {
            return state.isLoading;
        },
        success (state) {
            return state.success;
        },
        message (state) {
            return state.message;
        },
    },
    mutations: {
        ['REGISTERING'](state) {
            state.isLoading = true;
            state.success = null;
            state.error = null;
        },
        ['REGISTERING_SUCCESS'](state, res) {
            state.isLoading = false;
            state.success = res.data.success;
            state.message = res.data.message;
        },
        ['REGISTERING_ERROR'](state, error) {
            state.isLoading = false;
            state.success = false;
            state.error = error;
        },
    },
    actions: {
        register ({commit}, payload) {
            commit('REGISTERING');
            return AuthApi.register(payload.login, payload.username, payload.password, payload.rules_accepted)
                .then(res => commit('REGISTERING_SUCCESS', res))
                .catch(err => commit('REGISTERING_ERROR', err));
        },
    },
}
