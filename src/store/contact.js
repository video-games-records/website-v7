import EmailApi from '@/services/api/email/Email';

export default {
    namespaced: true,
    state: {
        isLoading: false,
        success : null,
        message: null,
    },
    getters: {
        isLoading (state) {
            return state.isLoading;
        },
        success (state) {
            return state.success;
        },
        message (state) {
            return state.message;
        },
    },
    mutations: {
        ['SENDING'](state) {
            state.isLoading = true;
            state.success = null;
            state.error = null;
        },
        ['SENDING_SUCCESS'](state, res) {
            state.isLoading = false;
            state.success = res.data.success;
            state.message = res.data.message;
        },
        ['SENDING_ERROR'](state, error) {
            state.isLoading = false;
            state.success = false;
            state.error = error;
        },
    },
    actions: {
        send ({commit}, payload) {
            commit('SENDING');
            return EmailApi.send(payload.email, payload.subject, payload.text)
                .then(res => commit('SENDING_SUCCESS', res))
                .catch(err => commit('SENDING_ERROR', err));
        },
    },
}
