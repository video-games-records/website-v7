export default {
    namespaced: true,
    state: {
        type: 'info', // info|error|confirm
        message: null,
        display: 0,
    },
    getters: {
        type (state) {
            return state.type;
        },
        message (state) {
            return state.message;
        },
        display (state) {
            return state.display;
        },
    },
    mutations: {
        ['INFO'](state, message) {
            state.type = 'info';
            state.message = message;
            state.display = 10;
        },
        ['CONFIRM'](state, message) {
            state.type = 'confirm';
            state.message = message;
            state.display = 10;
        },
        ['ERROR'](state, message) {
            state.type = 'error';
            state.message = message;
            state.display = 10;
        },
        ['SET_DISPLAY'] (state, display) {
            state.display = display
        },
    },
    actions: {
        info({ commit }, message) {
            commit('INFO', message);
        },
        confirm({ commit }, message) {
            commit('CONFIRM', message);
        },
        error({ commit }, message) {
            commit('ERROR', message);
        },
        display({ commit }, display) {
            commit('SET_DISPLAY', display);
        },
    },
}
