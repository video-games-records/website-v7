export default {
    namespaced: true,
    state: {
        level: 0,
        item0: {
            text: 'Home',
            to : { name: 'Home'}
        },
        item1: {},
        item2: {},
        item3: {},
        item4: {},
        item5: {},
    },
    getters: {
        item0 (state) {
            return state.item0;
        },
        item1 (state) {
            return state.item1;
        },
        item2 (state) {
            return state.item2;
        },
        item3 (state) {
            return state.item3;
        },
        item4 (state) {
            return state.item4;
        },
        item5 (state) {
            return state.item5;
        },
        level (state) {
            return state.level;
        },
    },
    mutations: {
        setItem1(state, item) {
            state.item1 = item;
        },
        setItem2(state, item) {
            state.item2 = item;
        },
        setItem3(state, item) {
            state.item3 = item;
        },
        setItem4(state, item) {
            state.item4 = item;
        },
        setItem5(state, item) {
            state.item5 = item;
        },
        setLevel(state, level) {
            state.level = level;
        },
        setOnlyItem1(state, item) {
            state.level = 1;
            state.item1 = item;
            state.item2 = {};
            state.item3 = {};
            state.item4 = {};
            state.item5 = {};
        }
    },
}
