import AuthApi from '../services/auth.service';

export default {
    namespaced: true,
    state: {
        isLoading: false,
        error: null,
        isAuthenticated: false,
        roles: [],
        user: {},
        player: {},
        refreshTokenPromise: null
    },
    getters: {
        isLoading (state) {
            return state.isLoading;
        },
        hasError (state) {
            return state.error !== null;
        },
        error (state) {
            return state.error;
        },
        isAuthenticated (state) {
            return state.isAuthenticated;
        },
        hasRole (state) {
            return role => {
                return state.roles.indexOf(role) !== -1;
            }
        },
        getUser(state) {
            return state.user;
        },
        getPlayer(state) {
            return state.player;
        }
    },
    mutations: {
        ['INIT'](state) {
            state.isLoading = false;
            state.error = null;
            state.isAuthenticated = false;
            state.roles = [];
            state.user = {};
            state.player = {};
        },
        ['AUTHENTICATING'](state) {
            state.isLoading = true;
            state.error = null;
            state.isAuthenticated = false;
            state.roles = [];
            state.user = {};
            state.player = {};
        },
        ['AUTHENTICATING_SUCCESS'](state, res) {
            state.isLoading = false;
            state.error = null;
            state.isAuthenticated = true;
            state.roles = res['user'][0];
            state.user = res['user'][1];
            state.player = res['player'];
        },
        ['AUTHENTICATING_ERROR'](state, error) {
            state.isLoading = false;
            state.error = error;
            state.isAuthenticated = false;
            state.roles = [];
            state.user = {};
            state.player = {};
        },
        ['LOGOUT'](state) {
            state.isLoading = false;
            state.error = null;
            state.isAuthenticated = false;
            state.roles = [];
            state.user = {};
            state.player = {};
        },
        ['REFRESH_TOKEN_PROMISE'](state, promise) {
            state.refreshTokenPromise = promise;
        }
    },
    actions: {
        init ({commit}) {
            commit('INIT');
        },
        login ({commit}, payload) {
            commit('AUTHENTICATING');
            return AuthApi.login(payload.login, payload.password)
                .then(res => commit('AUTHENTICATING_SUCCESS', res))
                .catch(err => commit('AUTHENTICATING_ERROR', err));
        },
        logout ({commit}) {
            return AuthApi.logout()
                .then(commit('LOGOUT'));
        },
        refreshToken({ commit, state }) {
            // If this is the first time the refreshToken has been called, make a request
            // otherwise return the same promise to the caller

            if(null === state.refreshTokenPromise) {
                const p = AuthApi.refreshToken()
                commit('REFRESH_TOKEN_PROMISE', p)

                // Wait for the AuthApi.refreshToken() to resolve. On success set the token and clear promise
                // Clear the promise on error as well.
                p.then(
                    // eslint-disable-next-line no-unused-vars
                    response => {
                        commit('REFRESH_TOKEN_PROMISE', null)
                    },
                    // eslint-disable-next-line no-unused-vars
                    error => {
                        commit('REFRESH_TOKEN_PROMISE', null)
                    }
                )
            }
            return state.refreshTokenPromise
        }
    },
}
