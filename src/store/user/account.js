import UserApi from '@/services/api/user/User';
import PlayerApi from '@/services/api/vgr/Player';
import ApiService from "@/services/api.service";

export default {
    namespaced: true,
    state: {
        isInitialized: false,
        isLoading: false,
        success: null,
        user: {},
        player: {},
    },
    getters: {
        isInitialized (state) {
            return state.isInitialized;
        },
        isLoading (state) {
            return state.isLoading;
        },
        hasError (state) {
            return state.success === false;
        },
        hasSuccess (state) {
            return state.success === true;
        },
        user (state) {
            return state.user;
        },
        player (state) {
            return state.player;
        },
    },
    mutations: {
        ['INIT'](state) {
            state.isInitialized = true;
        },
        ['SET_USER'](state, user) {
            state.user = user;
        },
        ['SET_PLAYER'](state, player) {
            state.player = player;
        },
        ['SUBMIT'](state) {
            state.isLoading = true;
            state.success = null;
            state.message = null;
        },
        ['CANCEL'](state) {
            state.isLoading = false;
            state.isInitialized = false;
            state.success = null;
            state.message = null;
        },
        ['ERROR'](state, message) {
            state.isLoading = false;
            state.success = false;
            state.message = message;
        },
        ['SUCCESS'](state, message) {
            state.isLoading = false;
            state.success = true;
            state.message = message;
        },
    },
    actions: {
        async update ({commit}, payload) {
            commit('SUBMIT');
            let promises = [];
            promises.push(await UserApi.put(payload.user));
            promises.push(await PlayerApi.put(payload.player));
            ApiService.all(promises).then(() => {
                commit('SUCCESS');
            });
        },
        init ({commit}) {
            commit('INIT');
        },
        setUser ({commit}, user) {
            commit('SET_USER', user);
        },
        setPlayer({commit}, player) {
            commit('SET_PLAYER', player);
        },
        cancel ({commit}) {
            commit('CANCEL');
        },
    },
}
