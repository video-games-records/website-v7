import MessageApi from '@/services/api/message/Message';

export default {
    namespaced: true,
    state: {
        nbMessage: 0,
        isLoading: false,
        message: {
            'object' : 'test',
            'message': null,
            'sender' : {
                'username' : null,
            },
            'recipient' : {
                'username' : null,
            },
        },
    },
    getters: {
        isLoading (state) {
            return state.isLoading;
        },
        getMessage(state) {
            return state.message;
        },
        getNbMessage(state) {
            return state.nbMessage;
        }
    },
    mutations: {
        ['READ_MESSAGE'](state, message) {
            state.isLoading = false;
            state.message = message;
        },
        ['SET_NB_MESSAGE'](state, nbMessage) {
            state.nbMessage = nbMessage;
        },
        ['DECREMENT'](state) {
            state.nbMessage = state.nbMessage - 1;
        },
    },
    actions: {
        read ({commit}, message) {
            commit('READ_MESSAGE', message);
        },
        loadNbMessage({commit}, idRecipient) {
            MessageApi.getNbNewMessage(idRecipient) .then(response => {
                commit('SET_NB_MESSAGE', response.data);
            });
        },
        decrement({commit}) {
            commit('DECREMENT');
        },
    },
}
