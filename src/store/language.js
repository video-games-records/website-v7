import i18n from '../i18n';

export default {
    namespaced: true,
    state: {
        language: localStorage.lang || 'en',
    },
    getters: {
        getLanguage (state) {
            return state.language;
        },
    },
    mutations: {
        ['SET_LANGUAGE'](state, language) {
            state.language = language;
            localStorage.lang = language;
            i18n.locale = language;
            document.documentElement.lang = language;
        },
    },
    actions: {
        switchI18n({ commit }, language) {
            commit('SET_LANGUAGE', language);
        },
    },
}
