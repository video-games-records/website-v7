export default {
    namespaced: true,
    state: {
        platform: {
            id: 52,
            slug: 'switch',
            libPlatform: 'Switch'
        },
        serie: {
            id: 2,
            slug: 'mario-kart',
            libPlatform: 'Mario Kart'
        },
        letter : '0',
        game: {
            platforms: [],
            forum: {},
            status: {
                value: null
            }
        },
        group: {},
        chart: {},
        playerChart: {
            player: {},
            status: {}
        },
        lostPosition: {
            idGame : null
        }
    },
    getters: {
        platform (state) {
            return state.platform;
        },
        serie (state) {
            return state.serie;
        },
        letter (state) {
            return state.letter;
        },
        game (state) {
            return state.game;
        },
        group(state) {
            return state.group;
        },
        chart(state) {
            return state.chart;
        },
        playerChart(state) {
            return state.playerChart;
        },
        lostPosition(state) {
            return state.lostPosition;
        },
        gamePlatformList(state) {
            let tab = [];
            state.game.platforms.forEach(function (platform) {
                tab.push(platform.libPlatform);
            });
            return '[' + tab.join(', ') + ']';
        }
    },
    mutations: {
        ['SET_PLATFORM'](state, platform) {
            state.platform = platform;
        },
        ['SET_SERIE'](state, serie) {
            state.serie = serie;
        },
        ['SET_LETTER'](state, letter) {
            state.letter = letter;
        },
        ['SET_GAME'](state, game) {
            state.game = game;
        },
        ['SET_GROUP'](state, group) {
            state.group = group;
        },
        ['SET_CHART'](state, chart) {
            state.chart = chart;
        },
        ['SET_PLAYER_CHART'](state, playerChart) {
            state.playerChart = playerChart;
        },
        ['SET_LOST_POSISTION_GAME'](state, idGame) {
            state.lostPosition.idGame = idGame;
        },
    },
    actions: {
        setPlatform ({commit}, platform) {
            commit('SET_PLATFORM', platform);
        },
        setSerie({commit}, serie) {
            commit('SET_SERIE', serie);
        },
        setLetter ({commit}, letter) {
            commit('SET_LETTER', letter);
        },
        setGame ({commit}, game) {
            commit('SET_GAME', game);
        },
        setGroup ({commit}, group) {
            commit('SET_GROUP', group);
        },
        setChart ({commit}, chart) {
            commit('SET_CHART', chart);
        },
        setPlayerChart ({commit}, playerChart) {
            commit('SET_PLAYER_CHART', playerChart);
        },
        setLostPositionGame({commit}, idGame) {
            commit('SET_LOST_POSISTION_GAME', idGame);
        },
    },
}
