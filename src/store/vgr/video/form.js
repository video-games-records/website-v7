import VideoApi from '@/services/api/vgr/Video';

export default {
    namespaced: true,
    state: {
        isInitialized: false,
        isLoading: false,
        success: null,
        message: null,
        violations: [],
        video: {
            libVideo: '',
            tag: null,
            url: null,
            game: null,
            player: null,
        },
    },
    getters: {
        isInitialized (state) {
            return state.isInitialized;
        },
        isLoading (state) {
            return state.isLoading;
        },
        hasError (state) {
            return state.success === false;
        },
        hasSuccess (state) {
            return state.success === true;
        },
        video (state) {
            return state.video;
        },
        message (state) {
            return state.message;
        },
    },
    mutations: {
        ['INIT'](state, video) {
            state.isInitialized = true;
            state.video = video;
        },
        ['SUBMIT'](state) {
            state.isLoading = true;
            state.success = null;
            state.message = null;
        },
        ['SUCCESS'](state, message) {
            state.isLoading = false;
            state.success = true;
            state.message = message;
        },
        ['ERROR'](state, message, violations) {
            state.isLoading = false;
            state.success = false;
            state.message = message;
            state.violations = violations;
        },
    },
    actions: {
        init ({commit}, video) {
            commit('INIT', video);
        },
        async post ({commit}, video) {
            commit('SUBMIT');
            await VideoApi.post(video).then(response => {
                if (response.status === 201) {
                    commit('SUCCESS');
                } else {
                    commit('ERROR', response.response.data['hydra:description'], response.response.data['violations']);
                }
            }).catch(error => {
                commit('ERROR', error.response.data['hydra:description'], error.response.data['violations']);
            });
        },
        async put ({commit}, video) {
            commit('SUBMIT');
            await VideoApi.put(video).then(response => {
                if (response.status === 200) {
                    commit('SUCCESS');
                } else {
                    commit('ERROR', response.response.data['hydra:description'], response.response.data['violations']);
                }
            }).catch(error => {
                commit('ERROR', error.response.data['hydra:description'], error.response.data['violations']);
            });
        },
    },
}
