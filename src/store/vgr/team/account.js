import AuthApi from '@/services/auth.service';
import TeamApi from '@/services/api/vgr/Team';
import PlayerApi from '@/services/api/vgr/Player';

export default {
    namespaced: true,
    state: {
        isInitialized: false,
        isLoading: false,
        success: null,
        message: null,
        violations: [],
        player: {},
        team: {},
    },
    getters: {
        isInitialized (state) {
            return state.isInitialized;
        },
        isLoading (state) {
            return state.isLoading;
        },
        hasError (state) {
            return state.success === false;
        },
        hasSuccess (state) {
            return state.success === true;
        },
        team (state) {
            return state.team;
        },
        player (state) {
            return state.player;
        },
        message (state) {
            return state.message;
        },
    },
    mutations: {
        ['INIT'](state, player) {
            state.isInitialized = true;
            state.player = player;
            if (player.team === null) {
                state.team = {};
            } else {
                state.team = player.team;
            }
        },
        ['SUBMIT'](state) {
            state.isLoading = true;
            state.success = null;
            state.message = null;
        },
        ['CANCEL'](state) {
            state.isLoading = false;
            state.isInitialized = false;
            state.success = null;
            state.message = null;
        },
        ['ERROR'](state, message, violations) {
            state.isLoading = false;
            state.success = false;
            state.message = message;
            state.violations = violations;
        },
        ['SUCCESS'](state, message) {
            state.isLoading = false;
            state.success = true;
            state.message = message;
        },
        ['RELOAD'](state) {
            state.isInitialized = false;
        },
    },
    actions: {
        init ({commit}) {
            AuthApi.playerProfile().then(player => {
                commit('INIT', player);
            });
        },
        async submit ({commit}, team) {
            commit('SUBMIT');
            if (team.id !== undefined) {
                await TeamApi.put(team).then(response => {
                    if (response.status === 200) {
                        commit('SUCCESS', 'team.update.success');
                    } else {
                        commit('ERROR', response.response.data['hydra:description']);
                    }
                }).catch(error => {
                    commit('ERROR', error.response.data['hydra:description'], error.response.data['violations']);
                });
            } else {
                await TeamApi.post(team).then(response => {
                    if (response.status === 201) {
                        commit('SUCCESS', 'team.insert.success');
                        commit('RELOAD');
                    } else {
                        commit('ERROR', response.response.data['hydra:description']);
                    }
                }).catch(error => {
                    commit('ERROR', error.response.data['hydra:description'], error.response.data['violations']);
                });
            }
        },
        async quit ({commit}, player) {
            commit('SUBMIT');
            await PlayerApi.put(player).then(response => {
                if (response.status === 200) {
                    commit('SUCCESS', 'quit_success');
                    commit('RELOAD');
                }
            });
        }
    },
}
