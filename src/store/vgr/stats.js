import PlayerApi from "@/services/api/vgr/Player";

export default {
    namespaced: true,
    state: {
        isLoading: false,
        isLoaded: false,
        data: [],
    },
    getters: {
        isLoading (state) {
            return state.isLoading;
        },
        isLoaded (state) {
            return state.isLoaded;
        },
        data(state) {
            return state.data;
        },
    },
    mutations: {
        ['LOADING'](state) {
            state.isLoading = true;
            state.isLoaded = false;
            state.data = [];
        },
        ['SUCCESS'](state, res) {
            state.isLoading = false;
            state.isLoaded = true;
            state.data = res;
        },
        ['RELOAD'](state) {
            state.isLoading = false;
            state.isLoaded = false;
            state.data = [];
        },
    },
    actions: {
        load ({commit}) {
            commit('LOADING');
            return PlayerApi.getStats()
                .then(res => commit('SUCCESS', res));
        },
    },
}
