export default {
    namespaced: true,
    state: {
        isInitialized: false,
        isLoading: false,
        charts: [],
        nbChartToUpdate: 0,
        nbChartUpdated: 0,
    },
    getters: {
        isInitialized (state) {
            return state.isInitialized;
        },
        isLoading (state) {
            return state.isLoading;
        },
        charts (state) {
            return state.charts;
        },
        nbChartToUpdate (state) {
            return state.nbChartToUpdate;
        },
        nbChartUpdated (state) {
            return state.nbChartUpdated;
        },
    },
    mutations: {
        ['LOADING'](state) {
            state.loading = true;
        },
        ['RESET'](state) {
            state.nbChartUpdated = 0;
        },
        ['SET_CHARTS'](state, charts) {
            state.isInitialized = true;
            state.loading = false;
            state.charts = charts;
            state.nbChartToUpdate = 0;
            state.nbChartUpdated = 0;
        },
        ['ADD_CHART_TO_UPDATE'](state) {
            state.nbChartToUpdate++;
        },
        ['ADD_CHART_UPDATED'](state) {
            state.nbChartUpdated++;
        },
        ['SUBMIT'](state) {
            state.isLoading = false;
            state.success = null;
            state.message = null;
        },

    },
    actions: {
        loading ({commit}) {
            commit('LOADING');
        },
        reset ({commit}) {
            commit('RESET');
        },
        setCharts ({commit}, charts) {
            commit('SET_CHARTS', charts);
        },
        addChartToUpdate ({commit}, chart) {
            commit('ADD_CHART_TO_UPDATE', chart);
        },
        addChartUpdated ({commit}) {
            commit('ADD_CHART_UPDATED');
        },
    },
}
