export default {
    namespaced: true,
    state: {
        players: [],
        games: [],
        platforms: []
    },
    getters: {
        players (state) {
            return state.players;
        },
        games (state) {
            return state.games;
        },
        platforms (state) {
            return state.platforms;
        },
    },
    mutations: {
        ['SET_PLAYERS'](state, players) {
            state.players = players;
        },
        ['SET_GAMES'](state, games) {
            state.games = games;
        },
        ['SET_PLATFORMS'](state, platforms) {
            state.platforms = platforms;
        },
    },
    actions: {
        setPlayers ({commit}, players) {
            commit('SET_PLAYERS', players);
        },
        setGames ({commit}, games) {
            commit('SET_GAMES', games);
        },
        setPlatforms ({commit}, platforms) {
            commit('SET_PLATFORMS', platforms);
        },
    },
}
