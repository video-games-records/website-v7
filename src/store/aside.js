export default {
    namespaced: true,
    state: {
        type: 'default',
        object: null,
    },
    getters: {
        type(state) {
            return state.type;
        },
        object (state) {
            return state.object;
        },
    },
    mutations: {
        ['SET_DEFAULT'](state) {
            state.type = 'default';
            state.object = null;
        },
        ['SET_FORUM'](state, forum) {
            state.type = 'forum';
            state.object = forum;
        },
        ['SET_PLATFORM'](state) {
            state.type = 'platform';
            state.object = null;
        },
        ['SET_SERIE'](state) {
            state.type = 'serie';
            state.object = null;
        },
        ['SET_GAME'](state, game) {
            state.type = 'game';
            state.object = game;
        },
    },
    actions: {
        setDefault ({commit}) {
            commit('SET_DEFAULT');
        },
        setForum ({commit}, forum) {
            commit('SET_FORUM', forum);
        },
        setPlatform ({commit}) {
            commit('SET_PLATFORM');
        },
        setSerie ({commit}) {
            commit('SET_SERIE');
        },
        setGame ({commit}, game) {
            commit('SET_GAME', game);
        },
    },
}
