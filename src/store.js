import Vue from 'vue';
import Vuex from 'vuex';
import VuexPersist from 'vuex-persist';

// General
import SecurityModule from './store/security';
import LanguageModule from './store/language';
import FlashMessageModule from './store/flashMessage';
import BreadcrumbsModule from './store/breadcrumbs';
import ContactModule from './store/contact';
import StatsModule from './store/vgr/stats';

// Navigation + Aside
import NavigationModule from './store/navigation';
import AsideModule from './store/aside.js';

// Message
import MessageModule from './store/message/message';

// User
import UserAccountModule from './store/user/account';

// User
import AuthenticationRegisterModule from './store/authentication/register';
import AuthenticationResetModule from './store/authentication/reset';

// VGR
import PlayerChartSubmitModule from './store/vgr/playerChart/submit';
import PlayerChartSearchModule from './store/vgr/playerChart/search';
import TeamAccountModule from './store/vgr/team/account';
import VideoFormModule from './store/vgr/video/form';

const vuexPersist = new VuexPersist({
    key: 'vgr-store',
    storage: localStorage,
    modules: [
        'security',
        'language',
        'platform',
        'playerChartSearch'
    ]
})

Vue.use(Vuex);

export default new Vuex.Store({
    plugins: [vuexPersist.plugin],
    modules: {
        // Authentication
        authenticationRegister: AuthenticationRegisterModule,
        authenticationReset: AuthenticationResetModule,
        // General
        security: SecurityModule,
        language: LanguageModule,
        flashMessage: FlashMessageModule,
        contact: ContactModule,
        stats: StatsModule,
        breadcrumbs: BreadcrumbsModule,
        // Navigation + Aside
        navigation: NavigationModule,
        aside: AsideModule,
        // Message
        message: MessageModule,
        // VGR
        playerChartSubmit: PlayerChartSubmitModule,
        playerChartSearch: PlayerChartSearchModule,
        TeamAccount: TeamAccountModule,
        videoForm: VideoFormModule,
        // User
        UserAccount: UserAccountModule,
    },
});
