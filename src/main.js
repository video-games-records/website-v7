import Vue from 'vue';
import vueHeadful from 'vue-headful';
import App from './App.vue';
import router from './router';
import i18n from './i18n';
import store from './store';
import ApiService from './services/api.service';
import CKEditor from '@ckeditor/ckeditor5-vue';
import VueYouTubeEmbed from 'vue-youtube-embed';
import LoadScript from 'vue-plugin-load-script';
import VueGtag from "vue-gtag";
import Ads from 'vue-google-adsense';
import { VAsync } from 'vuetensils/src/components';
import { VDialog } from 'vuetensils/src/components';
import { VTabs } from 'vuetensils/src/components';
import SmartTable from 'vuejs-smart-table'
import { number } from 'vuetensils/src/filters';
import Loading from '@/components/Loading';
import HighchartsVue from 'highcharts-vue'


Vue.use(VueGtag, {
  config: {
    id: process.env.VUE_APP_GOOGLE_ANALYTICS_ID,
    params: {
      anonymize_ip: true
    }
  }
}, router);

Vue.config.productionTip = false;
Vue.use( CKEditor );

if (localStorage.lang === undefined) {
  localStorage.lang = 'en';
}
i18n.locale = localStorage.lang;

// Set the base URL of the API
ApiService.init(process.env.VUE_APP_ROOT_API);
ApiService.setHeader();
ApiService.setCache();

// Youtube & twitch
Vue.use(VueYouTubeEmbed)
Vue.use(LoadScript);

// Google Adsense
Vue.use(require('vue-script2'));
Vue.use(Ads.Adsense);

Vue.use(SmartTable)

const customNumber = str => number(str, i18n.locale );
Vue.filter('number', customNumber);
Vue.component('VAsync', VAsync);
Vue.component('VDialog', VDialog);
Vue.component('VTabs', VTabs);
Vue.component('vue-headful', vueHeadful);

Vue.component('Loading', Loading);

Vue.use(HighchartsVue);

Vue.prototype.$GAME_STATUS_ACTIVE = 'ACTIVE'
Vue.prototype.$SERIE_STATUS_ACTIVE = 'ACTIVE'

new Vue({
  router,
  i18n,
  store,
  render: h => h(App)
}).$mount('#app')
