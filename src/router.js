import Vue from 'vue';
import VueRouter from 'vue-router';
import { wrapRouter } from "oaf-vue-router";
import Store from './store';
import i18n from './i18n';
import ApiService from './services/api.service';

// 404
import NotFound from './views/NotFound.vue';


// VGR
import GameIndex from './views/game/Index';
import GroupIndex from './views/game/group/Index';
import GroupSubmit from './views/game/group/Submit';
import ChartIndex from './views/game/group/chart/Index';
import ChartSubmit from './views/game/group/chart/Submit';


Vue.use(VueRouter)

let router = new VueRouter({
  mode: 'history',
  routes: [{
    path: '/:lang(fr|en)', component: {
       render(h) { return h('router-view'); }
    },
    children:[
      { path: '', name: 'Home', meta: {aside: true}, component: () => import(/* webpackChunkName: "Search" */ './views/Home.vue'),},
      { path: 'search', name: 'Search', meta: {aside: true}, component: () => import(/* webpackChunkName: "Search" */ './views/Search.vue'),},
      //Authentication
      { path: 'auth/login', name: 'AuthLogin', component: () => import(/* webpackChunkName: "Authentication" */ './views/authentication/Login.vue'),},
      { path: 'auth/reset', name: 'AuthReset', component: () => import(/* webpackChunkName: "Authentication" */ './views/authentication/Reset.vue'),},
      { path: 'register', name: 'Register', component: () => import(/* webpackChunkName: "Authentication" */ './views/authentication/Register.vue'),},

      { path: 'account', name: 'account', meta: {requiresAuth: true, aside: true}, component: () => import(/* webpackChunkName: "account" */ './views/account/Main.vue'),
        children: [
          { path: 'index', name: 'AccountIndex', component: () => import(/* webpackChunkName: "Player" */ './views/account/Index.vue')},
          { path: 'avatar', name: 'AccountAvatar', component: () => import(/* webpackChunkName: "Player" */ './views/account/Avatar.vue')},
          { path: 'team', name: 'AccountTeam', component: () => import(/* webpackChunkName: "Player" */ './views/account/Team.vue')},
          { path: 'badges', name: 'AccountBadges', component: () => import(/* webpackChunkName: "Player" */ './views/account/Badges.vue')},
          { path: 'videos', name: 'AccountVideos', component: () => import(/* webpackChunkName: "Player" */ './views/account/Videos.vue')},
        ]
      },
      { path: 'proofs', name: 'proof', meta: {requiresAuth: true, aside: true}, component: () => import(/* webpackChunkName: "account" */ './views/proof/Main.vue'),
        children: [
          { path: 'index', name: 'ProofIndex', component: () => import(/* webpackChunkName: "Player" */ './views/proof/Index.vue')},
          { path: ':slugGame-game-g:idGame(\\d+)', name: 'ProofGameMain', component: () => import(/* webpackChunkName: "Player" */ './views/proof/game/Main.vue'),
            children: [
              { path: 'proofs', name: 'ProofGameProofs', component: () => import(/* webpackChunkName: "Player" */ './views/proof/game/Proofs.vue')},
            ]
          },
        ]
      },
      { path: 'contact', name: 'Contact', meta: {aside: true}, component: () => import(/* webpackChunkName: "rules" */ './views/Contact.vue')},
      { path: 'rules', name: 'Rules', meta: {aside: true}, component: () => import(/* webpackChunkName: "rules" */ './views/page/Rules.vue')},
      { path: 'faq', name: 'Faq', meta: {aside: true}, component: () => import(/* webpackChunkName: "faq" */ './views/page/Faq.vue')},
      { path: 'partnership', name: 'Partnership', meta: {aside: true}, component: () => import(/* webpackChunkName: "partnership" */ './views/page/Partnership.vue')},
      { path: 'credits', name: 'Credits', meta: {aside: true}, component: () => import(/* webpackChunkName: "credits" */ './views/page/Credits.vue')},
      { path: 'recruitment', name: 'Recruitment', meta: {aside: true}, component: () => import(/* webpackChunkName: "recruitment" */ './views/page/Recruitment.vue')},
      { path: 'how-it-works', name: 'HowItWorks', meta: {aside: true}, component: () => import(/* webpackChunkName: "how-it-works" */ './views/page/HowItWorks.vue')},
      { path: 'donations', name: 'Donations', meta: {aside: true}, component: () => import(/* webpackChunkName: "donations" */ './views/page/Donations.vue')},
      { path: 'the-vgr-team', name: 'TheVgrTeam', meta: {aside: true}, component: () => import(/* webpackChunkName: "donations" */ './views/page/TheVgrTeam.vue')},
      /** VGR **/
      { path: 'leaderboard-point-chart', name: 'LeaderboardPointChart', meta: {aside: true}, component: () => import(/* webpackChunkName: "leaderboard" */ './views/leaderboard/PointChart.vue')},
      { path: 'leaderboard-point-game', name: 'LeaderboardPointGame', meta: {aside: true}, component: () => import(/* webpackChunkName: "leaderboard" */ './views/leaderboard/PointGame.vue')},
      { path: 'leaderboard-medal', name: 'LeaderboardMedal', meta: {aside: true}, component: () => import(/* webpackChunkName: "leaderboard" */ './views/leaderboard/Medal.vue')},
      { path: 'leaderboard-cup', name: 'LeaderboardCup', meta: {aside: true}, component: () => import(/* webpackChunkName: "leaderboard" */ './views/leaderboard/Cup.vue')},
      { path: 'leaderboard-proof', name: 'LeaderboardProof', meta: {aside: true}, component: () => import(/* webpackChunkName: "leaderboard" */ './views/leaderboard/Proof.vue')},
      { path: 'leaderboard-badge', name: 'LeaderboardBadge', meta: {aside: true}, component: () => import(/* webpackChunkName: "leaderboard" */ './views/leaderboard/Badge.vue')},
      { path: 'game-letter-:letter', name: 'GameListByLetter', meta: {aside: true}, component: () => import(/* webpackChunkName: "game-list" */ './views/game/list/ByLetter.vue'), props: {default: true}},
      { path: 'game-platform-:slugPlatform-p:id(\\d+)', name: 'PlatformMain', component: () => import(/* webpackChunkName: "platform" */ './views/platform/Main.vue'), props: {default: true},
        children: [
          { path: 'index', name: 'PlatformIndex', component: () => import(/* webpackChunkName: "game-list" */ './views/platform/Index.vue')},
          { path: 'leaderboard', name: 'PlatformLeaderboard', component: () => import(/* webpackChunkName: "game-list" */ './views/platform/Leaderboard.vue')},
        ]
      },
      { path: 'serie-:slugSerie-s:idSerie(\\d+)', name: 'SerieMain', component: () => import(/* webpackChunkName: "serie" */ './views/serie/Main.vue'), props: {default: true},
        children: [
          { path: 'leaderboard', name: 'SerieLeaderboard', component: () => import(/* webpackChunkName: "serie" */ './views/serie/Leaderboard.vue')},
        ]
      },
      { path: 'upcoming-games', name: 'GameNext', meta: {aside: true}, component: () => import(/* webpackChunkName: "game-list" */ './views/game/list/Next.vue')},
      { path: 'lastest-games', name: 'GameLast', meta: {aside: true}, component: () => import(/* webpackChunkName: "game-list" */ './views/game/list/Last.vue')},
      { path: 'last-submit', name: 'PlayerChartLast', component: () => import(/* webpackChunkName: "game-list" */ './views/playerChart/Last.vue')},
      { path: 'advanced-score-search', name: 'PlayerChartSearch', component: () => import(/* webpackChunkName: "game-list" */ './views/playerChart/Search.vue')},
      { path: ':slugGame-game-g:idGame(\\d+)', name: 'GameMain', component: () => import(/* webpackChunkName: "Game" */ './views/game/Main.vue'),
        children: [
          { path: 'index', name: 'GameIndex', component: GameIndex},
          { path: 'video-submit', name: 'GameVideoSubmit', meta: {requiresAuth: true}, component: () => import(/* webpackChunkName: "Video" */ './views/game/video/Submit.vue')},
          { path: 'videos', name: 'GameVideoList', component: () => import(/* webpackChunkName: "Video" */ './views/game/video/List.vue')},
          { path: 'rules', name: 'GameRules', component: () => import(/* webpackChunkName: "Video" */ './views/game/Rules.vue')},
          { path: 'submit', name: 'GameSubmit', component: () => import(/* webpackChunkName: "Game" */ './views/game/Submit.vue')},
          { path: ':slugForum-forum-f:idForum(\\d+)', name: 'GameForumMain', component: () => import(/* webpackChunkName: "Video" */ './views/game/forum/Main.vue'),
            children: [
              { path: 'index', name: 'GameForumIndex', component: () => import(/* webpackChunkName: "forum" */ './views/game/forum/Index.vue')},
              { path: ':slugTopic-topic-t:idTopic(\\d+)', name: 'GameTopicMain', component: () => import(/* webpackChunkName: "forum" */ './views/game/forum/topic/Main.vue'),
                children: [
                  { path: 'index', name: 'GameTopicIndex', component: () => import(/* webpackChunkName: "forum" */ './views/game/forum/topic/Index.vue')},
                  { path: 'reply', name: 'GameTopicReply', meta: {requiresAuth: true}, component: () => import(/* webpackChunkName: "forum" */ './views/game/forum/topic/Reply.vue')},
                ]
              },
              { path: 'new-topic', name: 'GameTopicNew', component: () => import(/* webpackChunkName: "forum" */ './views/game/forum/NewTopic.vue')},
            ]
          },
          { path: ':slugGroup-group-g:idGroup(\\d+)', name: 'GroupMain', component: () => import(/* webpackChunkName: "Game" */ './views/game/group/Main.vue'),
            children: [
              { path: 'index', name: 'GroupIndex', component: GroupIndex},
              { path: 'submit', name: 'GroupSubmit', meta: {requiresAuth: true}, component: GroupSubmit},
              { path: ':slugChart-chart-c:idChart(\\d+)', name: 'ChartMain', component: () => import(/* webpackChunkName: "Game" */ './views/game/group/chart/Main.vue'),
                children: [
                  { path: 'index', name: 'ChartIndex', component: ChartIndex},
                  { path: 'submit', name: 'ChartSubmit', meta: {requiresAuth: true}, component: ChartSubmit},
                  { path: 'pc-:idPc(\\d+)', name: 'PlayerChartMain', component: () => import(/* webpackChunkName: "Game" */ './views/game/group/chart/playerChart/Main.vue'),
                    children: [
                      { path: 'index', name: 'PlayerChartIndex', component: () => import(/* webpackChunkName: "Game" */ './views/game/group/chart/playerChart/Index.vue')},
                    ]
                  }
                ]
              }
            ]
          }
        ]
      },
      /** VGR VIDEO **/
      { path: 'videos', name: 'VideoList', meta: {aside: true}, component: () => import(/* webpackChunkName: "Video" */ './views/vgr/video/List.vue')},
      { path: ':slugVideo-video-v:idVideo', meta: {aside: true}, name: 'VideoIndex', component: () => import(/* webpackChunkName: "Video" */ './views/vgr/video/Index.vue')},
      /** VGR PLAYER **/
      { path: ':slugPlayer-player-p:idPlayer(\\d+)', name: 'PlayerMain', meta: {aside: true}, component: () => import(/* webpackChunkName: "Game" */ './views/player/Main.vue'),
        children: [
          { path: 'index', name: 'PlayerIndex', component: () => import(/* webpackChunkName: "Player" */ './views/player/Index.vue')},
          { path: 'badges', name: 'PlayerBadges', component: () => import(/* webpackChunkName: "Player" */ './views/player/Badges.vue')},
          { path: 'games', name: 'PlayerGames', component: () => import(/* webpackChunkName: "Player" */ './views/player/Games.vue')},
          { path: 'proofs', name: 'PlayerProofs', component: () => import(/* webpackChunkName: "Player" */ './views/player/Proofs.vue')},
          { path: 'charts', name: 'PlayerCharts', component: () => import(/* webpackChunkName: "Player" */ './views/player/Charts.vue')},
          { path: 'messages', name: 'PlayerMessages', component: () => import(/* webpackChunkName: "Player" */ './views/player/Messages.vue')},
          { path: 'presentation', name: 'PlayerPresentation', component: () => import(/* webpackChunkName: "Player" */ './views/player/Presentation.vue')},
          { path: 'collection', name: 'PlayerCollection', component: () => import(/* webpackChunkName: "Player" */ './views/player/Collection.vue')},
          { path: ':slugGame-game-g:idGame(\\d+)', name: 'PlayerGameMain', component: () => import(/* webpackChunkName: "Player" */ './views/player/game/Main.vue'),
            children: [
              { path: 'proofs', name: 'PlayerGameProofs', component: () => import(/* webpackChunkName: "Player" */ './views/player/game/Proofs.vue')},
            ]
          },
        ]
      },
      { path: 'player/list', name: 'PlayerList', meta: {aside: true}, component: () => import(/* webpackChunkName: "Game" */ './views/player/List.vue')},
      { path: 'lost-position', name: 'LostPosition', meta: {requiresAuth: true, aside: true}, component: () => import(/* webpackChunkName: "lost-position" */ './views/vgr/LostPosition.vue')},
      /** VGR TEAM **/
      { path: ':slugTeam-team-t:idTeam(\\d+)', name: 'TeamMain', meta: {aside: true}, component: () => import(/* webpackChunkName: "Team" */ './views/team/Main.vue'),
        children: [
          { path: 'index', name: 'TeamIndex', component: () => import(/* webpackChunkName: "Player" */ './views/team/Index.vue')},
          { path: 'presentation', name: 'TeamPresentation', component: () => import(/* webpackChunkName: "Player" */ './views/team/Presentation.vue')},
          { path: 'badges', name: 'TeamBadges', component: () => import(/* webpackChunkName: "Player" */ './views/team/Badges.vue')},
          { path: 'games', name: 'TeamGames', component: () => import(/* webpackChunkName: "Player" */ './views/team/Games.vue')},
          { path: 'players', name: 'TeamPlayers', component: () => import(/* webpackChunkName: "Player" */ './views/team/Players.vue')},
          { path: 'leaderboards', name: 'TeamLeaderboards', component: () => import(/* webpackChunkName: "Player" */ './views/team/Leaderboards.vue')},
        ]
      },
      /** FORUM **/
      { path: 'forum-home', name: 'ForumHome', component: () => import(/* webpackChunkName: "forum" */ './views/forum/Home.vue')},
      { path: 'forum-notify', name: 'ForumNotify', meta: {requiresAuth: true}, component: () => import(/* webpackChunkName: "forum" */ './views/forum/forum/Notify.vue')},
      { path: ':slugForum-forum-f:idForum(\\d+)', name: 'ForumMain', component: () => import(/* webpackChunkName: "forum" */ './views/forum/forum/Main.vue'),
        children: [
          { path: 'index', name: 'ForumIndex', component: () => import(/* webpackChunkName: "forum" */ './views/forum/forum/Index.vue')},
          { path: ':slugTopic-topic-t:idTopic(\\d+)', name: 'TopicMain', component: () => import(/* webpackChunkName: "forum" */ './views/forum/forum/topic/Main.vue'),
            children: [
              { path: 'index', name: 'TopicIndex', component: () => import(/* webpackChunkName: "forum" */ './views/forum/forum/topic/Index.vue')},
              { path: 'reply', name: 'TopicReply', meta: {requiresAuth: true}, component: () => import(/* webpackChunkName: "forum" */ './views/forum/forum/topic/Reply.vue')},
            ]
          },
          { path: 'new-topic', name: 'TopicNew', component: () => import(/* webpackChunkName: "forum" */ './views/forum/forum/NewTopic.vue')},
        ]
      },
      /** MESSAGE **/
      { path: 'messages', name: 'MessageIndex', meta: {requiresAuth: true, aside:true}, component: () => import(/* webpackChunkName: "message" */ './views/message/Index.vue')},
      /** ARTICLE **/
      { path: ':slugArticle-article-a:id(\\d+)', name: 'ArticleIndex', meta: {aside: true}, component: () => import(/* webpackChunkName: "article" */ './views/article/Index.vue')},
      { path: 'articles', name: 'ArticleList', meta: {aside: true}, component: () => import(/* webpackChunkName: "article" */ './views/article/List.vue')},
      { path: '*', component: NotFound }
    ]
  }]
});

const settings = {
  primaryFocusTarget: "skip-link",
  documentTitle: () => document.title,
  // BYO localization
  //shouldHandleAction: (previousRoute, nextRoute) => true,
  announcePageNavigation: false,
  setPageTitle: false,
  smoothScroll: false,
};

wrapRouter(router, settings);

router.beforeEach((to, from, next) => {

  // Get the save language if it exists
  let lang = localStorage.lang ? localStorage.lang.toLowerCase() : 'en';

  const toLang = to.params && to.params.lang;
  const fromLang = from.params && from.params.lang;

  // Aside
  if (to.matched.some(record => record.meta.aside)) {
    Store.dispatch('aside/setDefault');
  }

  if (toLang === undefined) {
    next(`/${lang}${to.fullPath}`);
  } else {
    lang = to.params.lang.toLowerCase();
    const supported_locales = process.env.VUE_APP_I18N_SUPPORTED_LOCALE.split(',');
    if (!supported_locales.includes(lang)) {
      lang = 'en';
    }
    i18n.locale = lang;

    if (fromLang !== toLang) {
      ApiService.setHeader();
    }

    if (to.matched.some(record => record.meta.requiresAuth)) {
      // this route requires auth, check if logged in
      // if not, redirect to login page.
      if (Store.getters['security/isAuthenticated']) {
        next();
      } else {
        next({
          path: '/auth/login',
          query: { redirect: to.fullPath }
        });
      }
    } else {
      next();
    }
  }
});

export default router;
