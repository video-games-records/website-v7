import ApiService from '../../api.service'

export default {
    getAll() {
        return ApiService.get('api/countries?pagination=false', {cache : {useCache: true}}).then(response => {
            return response.data['hydra:member'];
        })
    },
}
