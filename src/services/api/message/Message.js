import ApiService from '../../api.service'

export default {
    post(message) {
        return ApiService.post('/api/messages', message).then(response => {
            return response;
        })
    },
    put(message) {
        return ApiService.put('/api/messages/' + message.id, message).then(response => {
            return response;
        })
    },
    getNbNewMessage() {
        return ApiService.get('/api/messages/get-nb-new-message').then(response => {
              return response;
        })
    },
    getRecipients() {
        return ApiService.get('/api/messages/get-recipients').then(response => {
            return response.data['hydra:member'];
        })
    },
    getSenders() {
        return ApiService.get('/api/messages/get-senders').then(response => {
            return response.data['hydra:member'];
        })
    },
}
