import ApiService from '../../api.service'

export default {
    post(request) {
        return ApiService.post('/api/team_requests', request).then(response => {
            return response;
        })
    },
    put(request) {
        return ApiService.put('/api/team_requests/' + request.id, request).then(response => {
            return response;
        })
    },
    getList(params = {}) {
        return ApiService.get('/api/team_requests', params).then(response => {
            return response.data['hydra:member']
        })
    }
}
