import ApiService from '../../api.service';

export default {
    getList(params = {}) {
        return ApiService.get('/api/data', params).then(response => {
            return response.data;
        })
    }
}
