import ApiService from '../../api.service'

export default {
    getList(params = {}) {
        return ApiService.get('/api/player_badges', params).then(response => {
            return response.data;
        })
    },
    put(playerBadge) {
        return ApiService.put('/api/player_badges/' + playerBadge.id, playerBadge).then(response => {
            return response;
        })
    },
}
