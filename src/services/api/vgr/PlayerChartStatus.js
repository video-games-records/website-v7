import ApiService from '../../api.service'

export default {
    getList(params = {}) {
        return ApiService.get('/api/player_chart_statuses', params).then(response => {
            return response.data;
        })
    }
}
