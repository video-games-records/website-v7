import ApiService from '../../api.service'

export default {
    getGame(id) {
        return ApiService.get('api/games/' + id).then(response => {
            return response.data;
        })
    },
    getStats() {
        return ApiService.get('api/games/stats').then(response => {
            return response.data;
        })
    },
    getGroups(id, params) {
        return ApiService.get('api/games/' + id + '/groups', params).then(response => {
            return response.data['hydra:member']
        })
    },
    getListByLetter(letter) {
        return ApiService.get('/api/games-list-by-letter?letter=' + letter).then(response => {
            return response.data;
        })
    },
    getPlayerRankingPoints(id, params = {}) {
        return ApiService.get('/api/games/' + id + '/player-ranking-points', params).then(response => {
              return response.data['hydra:member']
        })
    },
    getPlayerRankingMedals(id, params = {}) {
        return ApiService.get('/api/games/' + id + '/player-ranking-medals', params).then(response => {
            return response.data['hydra:member']
        })
    },
    getTeamRankingPoints(id, params = {}) {
        return ApiService.get('/api/games/' + id + '/team-ranking-points', params).then(response => {
            return response.data['hydra:member']
        })
    },
    getTeamRankingMedals(id, params = {}) {
        return ApiService.get('/api/games/' + id + '/team-ranking-medals', params).then(response => {
            return response.data['hydra:member']
        })
    },
    getList(params = {}) {
        return ApiService.get('/api/games', params).then(response => {
            return response.data;
        })
    }
}
