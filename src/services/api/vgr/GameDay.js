import ApiService from '../../api.service'

export default {
    getList(params = {}) {
        return ApiService.get('/api/game_days', params).then(response => {
            return response.data;
        })
    }
}
