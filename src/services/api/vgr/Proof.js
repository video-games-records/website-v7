import ApiService from '../../api.service'

export default {
    put(proof) {
        return ApiService.put('/api/proofs/' + proof.id, proof).then(response => {
            return response;
        })
    },
}
