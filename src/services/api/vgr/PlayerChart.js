import ApiService from '../../api.service'

export default {
    getList(params = {}) {
        return ApiService.get('/api/player_charts', params).then(response => {
            return response.data;
        })
    },
    getPlayerChart(id) {
        return ApiService.get('api/player_charts/' + id).then(response => {
            return response.data;
        })
    },
    post(playerChart) {
        return ApiService.post('/api/player_charts', playerChart).then(response => {
            return response;
        })
    },
    put(playerChart) {
        return ApiService.put('/api/player_charts/' + playerChart.id, playerChart).then(response => {
            return response;
        })
    },
    proveWithPicture(id, file) {
        const requestData = {
            method: 'post',
            url: "api/player-charts/" + id +"/send-picture",
            data: {
                file : file
            }
        }

       return ApiService.customRequest(requestData).then(response => {
            return response;
       })
    },
    proveWithVideo(id, url) {
        const requestData = {
            method: 'post',
            url: "api/player-charts/" + id +"/send-video",
            data: {
                url : url
            }
        }

        return ApiService.customRequest(requestData).then(response => {
            return response;
        })
    },
    majPlatform(idGame, idPlatform) {
        const requestData = {
            method: 'post',
            url: "api/player-charts/maj-platform",
            data: {
                idGame : idGame,
                idPlatform : idPlatform
            }
        }

        return ApiService.customRequest(requestData).then(response => {
            return response;
        })
    }
}
