import ApiService from '../../api.service';

export default {
    post(comment) {
        return ApiService.post('/api/video_comments', comment).then(response => {
            return response;
        })
    },
}
