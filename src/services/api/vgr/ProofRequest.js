import ApiService from '../../api.service'

export default {
    post(request) {
        return ApiService.post('/api/proof_requests', request).then(response => {
            return response;
        })
    },
}
