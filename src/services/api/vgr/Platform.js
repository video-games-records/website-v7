import ApiService from '../../api.service';

export default {
    getPlatform(id) {
        return ApiService.get('api/platforms/' + id).then(response => {
            return response.data;
        })
    },
    getList(params = {}) {
        return ApiService.get('/api/platforms', params).then(response => {
            return response.data;
        })
    },
    getPlatforms() {
        return ApiService.get(
            '/api/platforms',
            {
                query: {
                    pagination: false,
                    status: 'ACTIF',
                },
                cache : {useCache: true}
            }).then(response => {
            return response.data['hydra:member']
        })
    },
    getPlayerRankingPoints(id, params = {}) {
        return ApiService.get('/api/platforms/' + id + '/player-ranking-point', params).then(response => {
            return response.data['hydra:member']
        })
    },
}
