import ApiService from '../../api.service'

export default {
    getSerie(id) {
        return ApiService.get('api/series/' + id).then(response => {
            return response.data;
        })
    },
    getList(params = {}) {
        return ApiService.get('/api/series', params).then(response => {
            return response.data;
        })
    },
    getPlayerRankingPoints(id, params = {}) {
        return ApiService.get('/api/series/' + id + '/player-ranking-points', params).then(response => {
            return response.data['hydra:member']
        })
    },
    getPlayerRankingMedals(id, params = {}) {
        return ApiService.get('/api/series/' + id + '/player-ranking-medals', params).then(response => {
            return response.data['hydra:member']
        })
    }
}
