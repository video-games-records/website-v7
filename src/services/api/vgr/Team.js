import ApiService from '../../api.service'

export default {
    getTeam(id, params = {}) {
        return ApiService.get('api/teams/' + id, params).then(response => {
            return response.data;
        })
    },
    post(team) {
        return ApiService.post('/api/teams', team).then(response => {
            return response;
        })
    },
    put(team) {
        return ApiService.put('/api/teams/' + team.id, team).then(response => {
            return response;
        })
    },
    getRankingPointChart(params = {}) {
        return ApiService.get('/api/teams/ranking-point-chart', params).then(response => {
            return response.data['hydra:member']
        })
    },
    getRankingPointGame(params = {}) {
        return ApiService.get('/api/teams/ranking-point-game', params).then(response => {
            return response.data['hydra:member']
        })
    },
    getRankingMedal(params = {}) {
        return ApiService.get('/api/teams/ranking-medal', params).then(response => {
            return response.data['hydra:member']
        })
    },
    getRankingCup(params = {}) {
        return ApiService.get('/api/teams/ranking-cup', params).then(response => {
            return response.data['hydra:member']
        })
    },
    getRankingBadge(params = {}) {
        return ApiService.get('/api/teams/ranking-badge', params).then(response => {
            return response.data['hydra:member']
        })
    },
    updloadAvatar(file) {
        const requestData = {
            method: 'post',
            url: "api/teams/upload-avatar",
            data: {
                file : file
            }
        }
        return ApiService.customRequest(requestData).then(response => {
            return response;
        })
    },
    getList(params = {}) {
        return ApiService.get('/api/teams', params).then(response => {
            return response.data;
        })
    }
}
