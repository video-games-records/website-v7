import ApiService from '../../api.service'

export default {
    getList(params = {}) {
        return ApiService.get('/api/lost_positions', params).then(response => {
            return response.data;
        })
    },
    delete(id) {
        return ApiService.delete('api/lost_positions/' + id).then(response => {
            return response;
        })
    },
}
