import ApiService from '../../api.service'

export default {
    post(video) {
        return ApiService.post('/api/videos', video).then(response => {
            return response;
        })
    },
    put(video) {
        return ApiService.put('/api/videos/' + video.id, video).then(response => {
            return response;
        })
    },
    getVideo(id) {
        return ApiService.get('api/videos/' + id).then(response => {
            return response.data;
        })
    },
    getComments(id) {
        return ApiService.get('api/videos/' + id + '/comments?pagination=false').then(response => {
            return response.data['hydra:member']
        })
    },
    getList(params = {}) {
        return ApiService.get('/api/videos', params).then(response => {
            return response.data;
        })
    }
}
