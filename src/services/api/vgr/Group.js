import ApiService from '../../api.service'

export default {
    getGroup(id) {
        return ApiService.get('api/groups/' + id).then(response => {
            return response.data;
        })
    },
    getCharts(id, params) {
        return ApiService.get('api/groups/' + id + '/charts', params).then(response => {
            return response.data['hydra:member']
        })
    },
    getPlayerRankingPoints(id, params = {}) {
        return ApiService.get('/api/groups/' + id + '/player-ranking-points', params).then(response => {
            return response.data['hydra:member']
        })
    },
    getPlayerRankingMedals(id, params = {}) {
        return ApiService.get('/api/groups/' + id + '/player-ranking-medals', params).then(response => {
            return response.data['hydra:member']
        })
    },
    getTeamRankingPoints(id, params = {}) {
        return ApiService.get('/api/groups/' + id + '/team-ranking-points', params).then(response => {
            return response.data['hydra:member']
        })
    },
    getTeamRankingMedals(id, params = {}) {
        return ApiService.get('/api/groups/' + id + '/team-ranking-medals', params).then(response => {
            return response.data['hydra:member']
        })
    },
    getTopScore(id) {
        return ApiService.get('/api/groups/' + id + '/top-score').then(response => {
            return response.data['hydra:member']
        })
    },
}
