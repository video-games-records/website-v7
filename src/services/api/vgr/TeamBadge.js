import ApiService from '../../api.service'

export default {
    getList(params = {}) {
        return ApiService.get('/api/team_badges', params).then(response => {
            return response.data
        })
    },
    put(teamBadge) {
        return ApiService.put('/api/team_badges/' + teamBadge.id, teamBadge).then(response => {
            return response;
        })
    },
}
