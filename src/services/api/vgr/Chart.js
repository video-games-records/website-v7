import ApiService from '../../api.service'

export default {
    getChart(id) {
        return ApiService.get('api/charts/' + id).then(response => {
            return response.data;
        })
    },
    getPlayerRanking(id) {
        return ApiService.get('/api/charts/' + id + '/player-ranking').then(response => {
            return response.data['hydra:member']
        })
    },
    getPlayerRankingDisabled(id) {
        return ApiService.get('/api/charts/' + id + '/player-ranking-disabled').then(response => {
            return response.data['hydra:member']
        })
    },
    getTeamRanking(id) {
        return ApiService.get('/api/charts/' + id + '/team-ranking').then(response => {
            return response.data['hydra:member']
        })
    },
    getPlayerRankingPoints(id, params = {}) {
        return ApiService.get('/api/charts/' + id + '/player-ranking-points', params).then(response => {
            return response.data['hydra:member']
        })
    },
    getFormData(id) {
        let url = '/api/charts/' + id + '/form-data';
        return ApiService.get(url).then(response => {
            return response.data['hydra:member']
        })
    }
}
