import ApiService from '../../api.service'

export default {
    getPlayer(id, params = {}) {
        return ApiService.get('api/players/' + id, params).then(response => {
            return response.data;
        })
    },
    put(player) {
        return ApiService.put('/api/players/' + player.id, player).then(response => {
            return response;
        })
    },
    getStats() {
        return ApiService.get('api/players/stats').then(response => {
            return response.data['hydra:member'];
        })
    },
    getRankingPointChart(params = {}) {
        return ApiService.get('/api/players/ranking-point-chart', params).then(response => {
            return response.data['hydra:member']
        })
    },
    getRankingPointGame(params = {}) {
        return ApiService.get('/api/players/ranking-point-game', params).then(response => {
            return response.data['hydra:member']
        })
    },
    getRankingMedal(params = {}) {
        return ApiService.get('/api/players/ranking-medal', params).then(response => {
            return response.data['hydra:member']
        })
    },
    getRankingCup(params = {}) {
        return ApiService.get('/api/players/ranking-cup', params).then(response => {
            return response.data['hydra:member']
        })
    },
    getRankingProof(params = {}) {
        return ApiService.get('/api/players/ranking-proof', params).then(response => {
            return response.data['hydra:member']
        })
    },
    getRankingBadge(params = {}) {
        return ApiService.get('/api/players/ranking-badge', params).then(response => {
            return response.data['hydra:member']
        })
    },
    getList(params = {}) {
        return ApiService.get('/api/players', params).then(response => {
            return response.data;
        })
    },
    getStatuses(id, params = {}) {
        return ApiService.get('/api/players/' + id + '/player-chart-stats', params).then(response => {
              return response.data['hydra:member']
        })
    },
    getGameStatuses(id, params = {}) {
        return ApiService.get('/api/players/' + id + '/game-stats', params).then(response => {
              return response.data['hydra:member']
        })
    },
    getNbNewLostPosition(id) {
        return ApiService.get('/api/players/' + id + '/nb-new-lost-position').then(response => {
              return response;
        })
    },
    canAskProof(id) {
        return ApiService.get('/api/players/' + id + '/can-ask-proof').then(response => {
              return response;
        })
    },
    getPositions(id) {
        return ApiService.get(
            '/api/dwh_players/get-positions',
            {
                query:{
                    idPlayer: id
                },
                cache : {useCache: true}
            }).then(response => {
              return response.data['hydra:member'];
        })
    },
    getMedalsInTime(id) {
        return ApiService.get(
            '/api/dwh_players/get-medals-by-time',
            {
                query:{
                    idPlayer: id
                },
                cache : {useCache: true}
            }).then(response => {
              return response.data['hydra:member'];
        })
    }
}
