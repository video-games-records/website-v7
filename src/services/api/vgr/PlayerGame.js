import ApiService from '../../api.service'

export default {
    getList(params = {}) {
        return ApiService.get('/api/player_games', params).then(response => {
            return response.data['hydra:member']
        })
    }
}
