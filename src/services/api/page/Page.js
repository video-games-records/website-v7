import ApiService from '../../api.service'

export default {
    getPage(id) {
        return ApiService.get('api/pages/' + id).then(response => {
            return response.data;
        })
    },
    getFromSlug(slug) {
        return ApiService.get('api/pages/' + '?slug=' + slug).then(response => {
            return response.data['hydra:member'][0];
        })
    },
}
