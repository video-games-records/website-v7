import ApiService from '../../api.service'

export default {
    getList(params = {}) {
        return ApiService.get('/api/partners', params).then(response => {
            return response.data['hydra:member'];
        })
    }
}
