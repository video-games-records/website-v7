import ApiService from '../../api.service'

export default {
    send(email, subject, text) {
        const requestData = {
            method: 'post',
            url: "api/emails/send",
            data: {
                email : email,
                subject: subject,
                message: text
            }
        }
        return ApiService.customRequest(requestData).then(response => {
            return response;
        })
    },
}
