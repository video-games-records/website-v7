import ApiService from '../../api.service'

export default {
    getUser(id) {
        return ApiService.get('api/users/' + id).then(response => {
            return response.data;
        })
    },
    put(user) {
        return ApiService.put('/api/users/' + user.id, user).then(response => {
            return response;
        })
    },
    updloadAvatar(file) {
        const requestData = {
            method: 'post',
            url: "api/users/upload-avatar",
            data: {
                file : file
            }
        }
        return ApiService.customRequest(requestData).then(response => {
            return response;
        })
    },
    getList(params = {}) {
        return ApiService.get('/api/users', params).then(response => {
            return response.data;
        })
    }
}
