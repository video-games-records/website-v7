import ApiService from '../../api.service'

export default {
    getList(params = {}) {
        return ApiService.get('/api/articles', params).then(response => {
            return response.data['hydra:member']
        })
    },
    getComments(id) {
        return ApiService.get('api/articles/' + id + '/comments?pagination=false').then(response => {
            return response.data['hydra:member']
        })
    },
    getArticle(id) {
        return ApiService.get('api/articles/' + id).then(response => {
            return response.data;
        })
    },
}
