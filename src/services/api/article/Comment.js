import ApiService from '../../api.service';

export default {
    post(comment) {
        return ApiService.post('/api/article_comments', comment).then(response => {
            return response;
        })
    },
    put(comment) {
        return ApiService.put('/api/article_comments/' + comment.id, comment).then(response => {
            return response;
        })
    },
}
