import ApiService from '../../api.service'

export default {
    getDonors() {
        return ApiService.get('api/donations-donors').then(response => {
            return response.data['hydra:member'];
        })
    },
}
