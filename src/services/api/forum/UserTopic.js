import ApiService from '../../api.service'

export default {
    put(userTopic) {
        return ApiService.put('/api/forum_topic_users/' + userTopic.id, userTopic).then(response => {
            return response;
        })
    },
}
