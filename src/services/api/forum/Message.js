import ApiService from '../../api.service'

export default {
    post(message) {
        return ApiService.post('/api/forum_messages', message).then(response => {
            return response;
        })
    },
    put(message) {
        return ApiService.put('/api/forum_messages/' + message.id, message).then(response => {
            return response;
        })
    },
    getList(params = {}) {
        return ApiService.get('/api/forum_messages', params).then(response => {
            return response.data;
        })
    }
}
