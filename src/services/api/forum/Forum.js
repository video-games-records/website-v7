import ApiService from '../../api.service'

export default {
    getForum(id) {
        return ApiService.get('api/forum_forums/' + id).then(response => {
            return response.data;
        })
    },
    getList(params = {}) {
        return ApiService.get('/api/forum_forums', params).then(response => {
            return response.data;
        })
    },
    readAll() {
        return ApiService.get('/api/forum_forums/read-all').then(response => {
            return response;
        })
    },
    read(id) {
        return ApiService.get('/api/forum_forums/' + id + '/read').then(response => {
            return response;
        })
    },
}
