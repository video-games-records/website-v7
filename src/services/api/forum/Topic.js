import ApiService from '../../api.service'

export default {
    getTopic(id) {
        return ApiService.get('api/forum_topics/' + id).then(response => {
            return response.data;
        })
    },
    post(topic) {
        return ApiService.post('/api/forum_topics', topic).then(response => {
            return response;
        })
    },
    getList(params = {}) {
        return ApiService.get('/api/forum_topics', params).then(response => {
            return response.data;
        })
    }
}
