import ApiService from '../../api.service'

export default {
    getHome() {
        return ApiService.get('/api/categorie/home').then(response => {
            return response.data['hydra:member'];
        })
    },
}
