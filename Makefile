# docker-compose
compose-upgrade: down pull up
up:
	docker-compose up -d
pull:
	docker-compose pull
down:
	docker-compose down

bash-web:
	docker-compose exec web bash
