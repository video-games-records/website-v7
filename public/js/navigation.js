(function () {
    /**
     * Show/hide menu on mobile 
     */
    var toggle_button = document.getElementById('mobile-menu-toggle');

    toggle_button.addEventListener('click', function () {
        if (this.parentNode.parentNode.classList.contains('site-navigation--open')) {
            this.parentNode.parentNode.classList.remove('site-navigation--open');
            this.parentNode.parentNode.querySelector('button').setAttribute('aria-expanded', 'false');
            document.body.classList.remove('menu-opened');
        } else {
            this.parentNode.parentNode.classList.add('site-navigation--open');
            this.parentNode.parentNode.querySelector('button').setAttribute('aria-expanded', 'true');
            document.body.classList.add('menu-opened');
        }
    });

    /**
     * Hide menu if a link is clicked 
     */
    var menu_opened = document.querySelector('.site-navigation');
    var menu_links = document.querySelectorAll('.site-menu a');

    var hide_menu = function() {
        if (menu_opened.classList.contains('site-navigation--open')) {
            menu_opened.classList.remove('site-navigation--open');
            document.getElementById('mobile-menu-toggle').setAttribute('aria-expanded', 'false');
            document.body.classList.remove('menu-opened');
        }
    }

    Array.prototype.forEach.call(menu_links, function(el){
        el.addEventListener('click', hide_menu, false);
    });

    /**
     * Show/hide search on mobile 
     */
    var search_button = document.getElementById('mobile-search-toggle');

    search_button.addEventListener('click', function () {
        document.getElementById('aside').scrollIntoView();
        document.getElementById('term').focus();
    });
})();
